==================================================
Source Control
==================================================

Bitbucket URL: https://bitbucket.org/liammccann1992/bt_graph_transversal

==================================================
Building and Running
==================================================

This coding test has been provided as a runnable jar and source code.

To run the JAR:
	1. Open command line
	2. Navigate to location of JAR
	3. Run "java -jar JarName.jar "GraphFileToUse.Txt" "StartingEmployee" "FinishingEmployee"

----OPTIONAL----
To find out more about how the system works without looking at the source code the program can be put in to debug mode.

To run the JAR in debug more:
	1. Open command line
	2. Navigate to location of JAR
	3. Run "java -jar JarName.jar "GraphFileToUse.Txt" "StartingEmployee" "FinishingEmployee" -d


==================================================
Documentation
==================================================
Some JavaDoc has been provided within the code base.


==================================================
Testing
==================================================
Within the project there is unit tests within package "tests"

The unit tests cover the main areas of the program.

These can be run by:
	1. Opening Eclipse
	2. File > Import
	3. General > Archive File
	4. Select SourceCode.zip provided
	5. Ensure project has been selected to import
	6. Finish