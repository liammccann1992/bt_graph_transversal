package exceptions;

/**
 * @author Liam McCann
 *	Thrown when the data format of an inputting file does not
 *         match the system specification
 */
public class DataFormatException extends Exception {
	private static final long serialVersionUID = 6640695686433412338L;

	public DataFormatException(String message) {
		super(message);
	}
}