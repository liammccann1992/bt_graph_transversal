package exceptions;

/**
 * @author Liam McCann
 *
 *	Thrown when Graph cannot find a path between Node A and B
 */
public class CannotFindPathException extends Exception {
	private static final long serialVersionUID = 6640695686433412338L;
	public CannotFindPathException(String message) {
        super(message);
    }
}