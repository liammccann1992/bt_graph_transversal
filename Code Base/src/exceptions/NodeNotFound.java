package exceptions;


/**
 * @author Liam McCann
 *
 *	Thrown when searching the graph for a specific node which does'nt exist
 */
public class NodeNotFound extends Exception {
	private static final long serialVersionUID = 6640695686433412338L;
	public NodeNotFound(String message) {
        super(message);
    }
}