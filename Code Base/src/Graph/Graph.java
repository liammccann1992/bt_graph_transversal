package Graph;

import io.Logging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import common.ListHelper;
import common.StringHandler;
import exceptions.CannotFindPathException;
import exceptions.NodeNotFound;
import models.iEdge;
import models.iNode;

public class Graph implements iGraph{

	private Map<Integer,iNode> nodes;
	
	public Graph(Map<Integer,iNode> _nodes, List<iEdge> _edges) throws NodeNotFound{
		Logging.LogLn("Creating graph");
		nodes = _nodes;
		
		if(_edges == null)
			return;
		
		for(iEdge edge : _edges){
			iNode current = getNode(edge.getId());
			iNode desentation = getNode(edge.getDestinationId());
			
			Logging.LogLn("Adding parent/child relationship between " + current + " and " + desentation);
			current.addParentNode(desentation);
			desentation.addChildNode(current);
		}
	}
	
	public List<iNode> getNodes(String name) throws NodeNotFound{
		name = StringHandler.PrepareNameForCompare(name);
		
		List<iNode> foundNodes = new ArrayList<iNode>();
		
		for (iNode currentNode : getNodes()) {
			String current = StringHandler.PrepareNameForCompare(currentNode.getName());

			if (name.equals(current))
				foundNodes.add(currentNode);
		}
		
		if(foundNodes.size() == 0)
			throw new NodeNotFound("Cannot find node: " + name);
		return foundNodes;
	}
	
	public iNode getNode(int id) throws NodeNotFound{
		for (iNode entry : getNodes()) {
			if (entry.getId() == id)
				return entry;
		}
		throw new NodeNotFound("Cannot find ID: " + id);
	}
	
	public boolean DoesNodeExist(String name) {
		try{
			getNodes(name);
			return true;
		}
		catch(NodeNotFound e){
			return false;
		}
	}

	public List<iNode> getDirections(int start, int finish) throws CannotFindPathException, NodeNotFound{
		iNode to = getNode(finish);
		iNode from = getNode(start);
		
		return getDirections(from,to);
	}
	
	public List<iNode> getDirections(iNode start, iNode finish) throws CannotFindPathException{
		 Set<Integer> vis = new HashSet<Integer>();
		 Map<iNode, iNode> prev = new HashMap<iNode, iNode>();
		
	    List<iNode> directions = new LinkedList<iNode>();
	    Logging.LogLn("Searching from " + start.toString()  + " to " + finish.toString());
	    Queue<iNode> q = new LinkedList<iNode>();
	    iNode current = start;
	    q.add(current);
	    vis.add(current.getId());
	    while(!q.isEmpty()){
	        current = q.remove();
	        Logging.LogLn("Current node: " + current.toString());
	        if (current.getId() == finish.getId()){
	        	
	        	
	            break;
	        }else{
	            for(iNode node : current.getAllAttachedNodes()){
	                if(!vis.contains(node.getId())){
	                    q.add(node);
	                    vis.add(node.getId());
	                    prev.put(node, current);
	                }
	            }
	        }
	    }
	    if (!current.equals(finish)){
	        throw new CannotFindPathException("Cannot find path between " + start + " AND " + finish);
	    }
	    for(iNode node = finish; node != null; node = prev.get(node)) {
	        directions.add(node);
	    }
	    
	    return ListHelper.<iNode>ReverseList(directions);
	}

	
	public List<iNode> getNodes() {
		 return new ArrayList<iNode>(nodes.values());
	}

	public boolean BFS(iNode rootNode, iNode toFind){
		Set<Integer> visited = new HashSet<Integer>();
		  Queue<iNode> queue = new LinkedList<iNode>();
		    queue.offer(rootNode);
		    
		    while(!queue.isEmpty()){
		    	//Get next node in the quuee
		    	iNode node = queue.poll();
		        
		        //If it what we are looking for return true
		        if(node.getId() == toFind.getId())
			    	   return true;
		        
		        //Get all edges
		        if(visited.contains(node.getId()))
		        	continue;
		        
		        System.out.println("Node: " + node.getName());
		        visited.add(node.getId());

		       for(iNode neighhobur : node.getChildNodes())
		    	   queue.offer(neighhobur);
		    }
		    return false;
	}
}
