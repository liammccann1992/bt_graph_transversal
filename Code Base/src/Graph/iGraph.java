package Graph;

import java.util.Collection;
import java.util.List;

import exceptions.CannotFindPathException;
import exceptions.NodeNotFound;
import models.iNode;

/**
 * @author njb11185
 *
 *         Interfacing defining how other parts of the program can use the Graph
 */
public interface iGraph {

	/**
	 * @param name
	 *            Name of node to search for
	 * @return List of iNodes with the same name
	 * @throws NodeNotFound
	 *             Thrown when unable to find any nodes
	 * 
	 *             Searching for nodes with given name
	 */
	public List<iNode> getNodes(String name) throws NodeNotFound;

	/**
	 * @param name
	 *            Name of node(s) we want to check for existence
	 * @return True if found, False if not found
	 * 
	 *         Simple boolean function to check if node(s) exist
	 */
	public boolean DoesNodeExist(String name);

	/**
	 * @param start
	 *            Id of starting node
	 * @param finish
	 *            Id of finishing node
	 * @return Path between start node and finish node
	 * @throws CannotFindPathException
	 *             Thrown when no path can be found
	 * @throws NodeNotFound
	 *             Thrown when invalid ID is given as parameter and no node can
	 *             be found due to that
	 * 
	 *             One of the most important functions within the system which
	 *             gives the program a list of nodes which tell the user how to
	 *             get from A > B in the graph
	 */
	public List<iNode> getDirections(int start, int finish)
			throws CannotFindPathException, NodeNotFound;

	/**
	 * @param start
	 *             iNode which the graph gets directions from
	 * @param finish
	 *            iNode which the graph aims to get directions to node
	 * @return Path between start node and finish node
	 * @throws CannotFindPathException
	 *             Thrown when no path can be found

	 * 
	 *             One of the most important functions within the system which
	 *             gives the program a list of nodes which tell the user how to
	 *             get from A > B in the graph
	 */
	public List<iNode> getDirections(iNode start, iNode finish)
			throws CannotFindPathException;

	/**
	 * @return Collection of nodes that exist within the graph
	 * 
	 * Returns all nodes within the graph
	 */
	public Collection<iNode> getNodes();

	/**
	 * @param rootNode iNode to start Breadth First Search from
	 * @param toFind iNode which search aims to find
	 * @return True if node found, false if node isn't found
	 * 
	 * Performs breadth first search from given rootNode
	 */
	public boolean BFS(iNode rootNode, iNode toFind);

	/**
	 * @param id ID of the node to be found
	 * @return iNode that has been found
	 * @throws NodeNotFound Thrown when no nodes where found
	 * 
	 * Searching for node with given ID
	 */
	public iNode getNode(int id) throws NodeNotFound;
}
