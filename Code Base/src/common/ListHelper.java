package common;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Liam McCann
 *
 *         List helper provides common used functions that the whole system can
 *         use for manipulated lists of any type
 */
public class ListHelper {

	/**
	 * Takes a list of Type N e.g [0,1,2,3,4,5] and reverses the order returning
	 * [5,4,3,2,1]
	 * 
	 * @param toReverse
	 *            List items in which the order has to be reversed
	 * @return The list of items in reverse order
	 */
	public static <N extends Object> List<N> ReverseList(List<N> toReverse) {
		List<N> reveresed = new ArrayList<N>();
		for (int i = toReverse.size() - 1; i >= 0; i--) {
			reveresed.add(toReverse.get(i));
		}
		return reveresed;

	}
}
