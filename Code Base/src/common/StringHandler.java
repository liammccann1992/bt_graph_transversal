package common;

/**
 * @author Liam McCann
 *
 *         This provides common string manipulate functions that the whole
 *         system can use
 */
public class StringHandler {
	/**
	 * As BT require string matching within this program. This function will
	 * strip case, leading spaces, trailing spaces and convert multiple spaces
	 * to one space
	 * 
	 * @param name
	 *            String to be prepared for comparing
	 * @return Prepared string in generic format
	 */
	public static String PrepareNameForCompare(String name) {
		if (name == null)
			return "";
		// Set to lower case, remove leading/trailing white spaces then RegEx to
		// replace multiple space with one
		return name.toLowerCase().trim().replaceAll(" +", " ");
	}
}
