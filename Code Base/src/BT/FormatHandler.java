package BT;

import io.Logging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import exceptions.DataFormatException;
import exceptions.NodeNotFound;
import Graph.Graph;
import Graph.iGraph;
import models.Edge;
import models.Employee;
import models.Node;
import models.iEdge;
import models.iEmployee;
import models.iNode;

/**
 * @author Liam McCann
 * 
 *         This class is responsible for all the major data conversions within
 *         the system
 * 
 *         e.g. Text to employees or String to integer
 *
 */
public class FormatHandler {

	/**
	 * Defines what separates the columns within the input files, Allows it to
	 * be changed quickly with min code changes
	 */
	private final String ColumnDeliminater = "[|]";

	/**
	 * Takes a group of lines generally taken from a text files and returns them
	 * in a lsit of objects that the system can manipulate and use
	 * 
	 * @param lines
	 *            Lines which have been retrieved from the input files
	 * @return List<iEmployee> A parsed set of employees from the parameter
	 * @throws DataFormatException
	 *             Throws when data format within the lines are incorrect e.g.
	 *             only one column or no employee ID
	 */
	public List<iEmployee> parseLines(List<String> lines)
			throws DataFormatException {
		Logging.LogLn("Parsing lines > employees");
		List<iEmployee> nodes = new ArrayList<iEmployee>();

		for (int i = 1; i < lines.size(); i++) {
			String line = lines.get(i);

			Logging.LogLn("Converting line: " + line);
			String[] lineSplit = line.split(ColumnDeliminater);

			Integer empId = ConvertToInt(lineSplit[1].trim());
			String name = lineSplit[2].trim();
			Integer manId = ConvertToInt(lineSplit[3].trim());
			Logging.LogLn("Employee Id: " + empId + " Name: " + name
					+ " Manager ID: " + manId);

			if (empId == null)
				throw new DataFormatException("Invalid Line: " + line);

			nodes.add(new Employee(name, empId, manId));
		}

		return nodes;

	}

	/**
	 * Takes a String and attempts to convert to Integer
	 * 
	 * @param s
	 *            String to be converted
	 * @return Null if conversion failed, Integer if successful
	 */
	private Integer ConvertToInt(String s) {
		try {
			return Integer.parseInt(s);
		} catch (NumberFormatException nfe) {
			return null;
		}
	}

	/**
	 * This function takes a path provided by the system and converts it to a
	 * user friendly string.
	 * 
	 * This function converts the path to the format requested by BT e.g
	 * 
	 * Name(ID) <- or Name(ID) ->
	 * 
	 * @param path
	 *            List of nodes e.g The path to be converted
	 * @return Sting user friendly format of the path
	 */
	public String NodesToDirections(List<iNode> path) {

		StringBuilder userFriendlyPath = new StringBuilder();

		for (int i = 0; i < path.size(); i++) {
			iNode current = path.get(i);
			iNode next = null;

			userFriendlyPath.append(current.getName() + "(" + current.getId()
					+ ") ");

			if (i + 1 < path.size())
				next = path.get(i + 1);

			if (next != null && current.hasSpecificChildNode(next))
				userFriendlyPath.append("<- ");
			else if (next != null && current.hasSpecificParentNode(next))
				userFriendlyPath.append("-> ");
		}
		return userFriendlyPath.toString();

	}

	/**
	 * Core of the system as it takes the employees and converts it in a graph
	 * 
	 * @param employees
	 *            List of employees to which have to me made in to a graph
	 * @return Full formatted and ready to use graph
	 * @throws NodeNotFound
	 *             thrown when adding an Edge and the node cannot be found this
	 *             can be caused by the manager Id not be a real employee ID
	 */
	public iGraph EmployeesToGraph(List<iEmployee> employees)
			throws NodeNotFound {
		// Take our list of employyes covert them to nodes and there
		// relationships to edges to allow use within the graph
		Map<Integer, iNode> nodes = new HashMap<Integer, iNode>();
		List<iEdge> edges = new ArrayList<iEdge>();

		Logging.LogLn("Coverting list of employees to nodes:");
		for (iEmployee employee : employees) {
			nodes.put(employee.getId(),
					new Node(employee.getName(), employee.getId()));
			Logging.LogLn("Converted: " + employee.getName() + "("
					+ employee.getId() + ")");
			if (employee.getManagerId() != null) {
				Logging.LogLn("Adding edges between: " + employee.getId()
						+ " and " + employee.getManagerId());
				edges.add(new Edge(employee.getId(), employee.getManagerId()));
			}

		}
		Logging.LogLn("Conversion complete");
		return new Graph(nodes, edges);
	}
}
