package BT;

import io.FileHandler;
import io.Logging;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import common.StringHandler;
import exceptions.CannotFindPathException;
import exceptions.DataFormatException;
import exceptions.NodeNotFound;
import Graph.iGraph;
import models.iEmployee;
import models.iNode;

public class StartUp {

	public static void main(String[] args) {
		if (args.length != 3 && args.length != 4) {
			System.out.println("Invalid arguements provided");
			System.exit(1);
		}

		String fileToImport = args[0];
		String source = args[1];
		String desentation = args[2];
		
		if(args.length == 4 && args[3].equals("-d"))
			Logging.SetShowLogs(true);

		Logging.LogLn("File To Import: " + fileToImport);
		Logging.LogLn("Source: " + source);
		Logging.LogLn("Desentation: " + desentation);
		
		source = StringHandler.PrepareNameForCompare(source);
		desentation = StringHandler.PrepareNameForCompare(desentation);

		// Create our file handler and prepare to read lines
		FileHandler fileHandler = new FileHandler();
		List<String> lines = new ArrayList<String>();

		// Append to read lines from file
		try {
			lines = fileHandler.ParseFile(fileToImport);
		} catch (FileNotFoundException e) {
			System.err.println("Could not find file: " + fileToImport);
			System.exit(1);
		}

		FormatHandler formatHandler = new FormatHandler();
		
		// Prepare to parse the lines in to "Employees"
		List<iEmployee> employees = new ArrayList<iEmployee>();

		try {
			employees = formatHandler.parseLines(lines);
		} catch (DataFormatException e) {
			System.err.println("Formatting error occurred: " + e.getMessage());
			System.exit(2);
		}

		
		// Take our list of employyes covert them to nodes and there
		// relationships to edges to allow use within the graph
		iGraph graph = null;
		try {
			graph = formatHandler.EmployeesToGraph(employees);
		} catch (NodeNotFound e1) {
			System.err.println("Graph building failed: " + e1.getMessage());
			System.exit(4);
		}

		// Check if there is more than one node with the same name
		List<iNode> sourceNodes;
		iNode sourceNode = null;
		try {
			sourceNodes = graph.getNodes(source);
			if (sourceNodes.size() > 1) {
				sourceNode = handleMultipleNodes(desentation,sourceNodes);
			} else {
				sourceNode = sourceNodes.get(0);
			}
		} catch (NodeNotFound e2) {
			System.err.println("'" + source + "' is not a valid employee");
			System.exit(1);
		}
		
	 

		List<iNode> destinationNodes;
		iNode destenationNode = null;
		try {
			destinationNodes = graph.getNodes(desentation);
			if (destinationNodes.size() > 1) {
				destenationNode = handleMultipleNodes(desentation,destinationNodes);
			} else {
				destenationNode = destinationNodes.get(0);
			}

		} catch (NodeNotFound e1) {
			System.err.println("'" + desentation + "' is not a valid employee");
			System.exit(2);;
		}
		
	
		List<iNode> directions = null;
		try {
			directions = graph.getDirections(sourceNode,
					destenationNode);
		} catch (CannotFindPathException e) {
			System.err.println(e.getMessage());
			System.exit(3);
		}
		String directionStrings = formatHandler.NodesToDirections(directions);
		System.out.println(directionStrings);

	}

	public static iNode handleMultipleNodes(String query, List<iNode> nodes) {
		System.out.println("Multiple employees found named " + query
				+ " please select which one you want:");
		for (int i = 0; i < nodes.size(); i++) {
			iNode currentNode = nodes.get(i);
			System.out.println("Type '" + i + "' for "
					+ currentNode.getName() + "("
					+ currentNode.getId() + ")");
		}

		Scanner in = new Scanner(System.in);
		
		try {
			int chooseNode = in.nextInt();
			
			if(chooseNode < 0 || chooseNode > nodes.size() -1)
				throw new Exception("Invalid choice");

			return nodes.get(chooseNode);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Your choiche was not valid");
			System.exit(1);
		}
		return null;
	}

}
