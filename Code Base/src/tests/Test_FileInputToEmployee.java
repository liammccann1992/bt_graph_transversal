package tests;


import java.io.FileNotFoundException;
import java.util.List;

import io.FileHandler;

import org.junit.Assert;
import org.junit.Test;

public class Test_FileInputToEmployee {

	@Test
	public void OneManager() {
		FileHandler fh = new FileHandler();
		try {
			List<String> parseFile = fh.ParseFile("OneEmployeeMangerOnly.txt");
			Assert.assertTrue(parseFile.size() == 2);
		} catch (FileNotFoundException e) {
		}
	}
	
	@Test
	public void OneEmployeeOneManager() {
		FileHandler fh = new FileHandler();
		try {
			List<String> parseFile = fh.ParseFile("OneEmployeeOneManger.txt");
			Assert.assertTrue(parseFile.size() == 3);
		} catch (FileNotFoundException e) {
		}
	}

	@Test(expected=FileNotFoundException.class)
	public void NoFile() throws FileNotFoundException {
		FileHandler fh = new FileHandler();
		fh.ParseFile("NOT REAL FILE.txt");
	}
}
