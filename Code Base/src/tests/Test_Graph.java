package tests;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Edge;
import models.Node;
import models.iEdge;
import models.iNode;

import org.junit.Assert;
import org.junit.Test;

import exceptions.CannotFindPathException;
import exceptions.NodeNotFound;
import Graph.Graph;
import Graph.iGraph;

public class Test_Graph {

	private iGraph createGraph() throws NodeNotFound{
		Map<Integer,iNode> nodes = new HashMap<Integer,iNode>();
		nodes.put(1,new Node("Test1",1));
		nodes.put(2,new Node("Test2",2));
		nodes.put(3,new Node("Test3",3));
		nodes.put(4,new Node("Test4",4));
		nodes.put(5,new Node("Test5",5));
		nodes.put(6,new Node("Test6",6));
		nodes.put(7,new Node("Test7",7));
		nodes.put(8,new Node("Test8",8));
		nodes.put(9,new Node("Test9",9));
		
		List<iEdge> edges = new ArrayList<iEdge>();
		edges.add(new Edge(1,2));
		edges.add(new Edge(2,4));
		edges.add(new Edge(2,5));
		edges.add(new Edge(1,3));
		edges.add(new Edge(3,5));
		edges.add(new Edge(3,6));
		edges.add(new Edge(6,7));
		edges.add(new Edge(6,8));
		
		
		return new Graph(nodes,edges);
	}

	@Test
	public void getNodeIntFound() throws NodeNotFound {
		iGraph graph = createGraph();
		Assert.assertTrue(graph.getNode(2).getId() == 2);
	}

	@Test(expected=NodeNotFound.class)
	public void getNodeIntNotFound() throws NodeNotFound {
		iGraph graph = createGraph();
		graph.getNode(40);
	}

	@Test
	public void DoesNodeExistYes() throws NodeNotFound {
		iGraph graph = createGraph();
		Assert.assertTrue(graph.DoesNodeExist("Test4"));
	}
	
	@Test
	public void DoesNodeExistNo() throws NodeNotFound {
		iGraph graph = createGraph();
		Assert.assertFalse(graph.DoesNodeExist("Test55"));
	}

	@Test
	public void getDirectionsStringsSuccess() throws CannotFindPathException, NodeNotFound {
		iGraph graph = createGraph();
		List<iNode> path = graph.getDirections(1, 5);
		
		Assert.assertEquals(path.size(), 3);
		Assert.assertEquals(path.get(0).getId(), 1);
		Assert.assertEquals(path.get(2).getId(), 5);
	}

	@Test(expected=CannotFindPathException.class)
	public void getDirectionsStringsNoPath() throws CannotFindPathException, NodeNotFound {
		iGraph graph = createGraph();
		graph.getDirections(1, 9);
	}

	
	@Test
	public void getDirectionsByNodeSuccess() throws CannotFindPathException, NodeNotFound {
		iGraph graph = createGraph();
		List<iNode> path = graph.getDirections(4, 8);

		Assert.assertEquals(path.size(), 6);		
		Assert.assertEquals(path.get(0).getId(), 4);
		Assert.assertEquals(path.get(5).getId(), 8);
	}

	@Test(expected=CannotFindPathException.class)
	public void getDirectionsByNodeNoPath() throws CannotFindPathException, NodeNotFound {
		iGraph graph = createGraph();
		iNode from = graph.getNode(4);
		iNode to = graph.getNode(9);
		graph.getDirections(from, to);
	}


	@Test
	public void GetNodesAll() throws NodeNotFound {
		iGraph graph = createGraph();
		Assert.assertTrue(graph.getNodes().size() == 9);
	}

}
