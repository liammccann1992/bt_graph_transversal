package tests;

import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;

import common.ListHelper;

public class Test_ListHelper {

	@Test
	public void testReverseStringList() {
		List<String> toReverse = new ArrayList<String>();
		toReverse.add("Test");
		toReverse.add("Test1");
		toReverse.add("Test2");
		toReverse.add("Test3");
		toReverse.add("Test4");
		toReverse.add("Test5");
		
		List<String> revered = ListHelper.ReverseList(toReverse);
		
		Assert.assertEquals(revered.size(), toReverse.size());
		
		for(int tr = 0 ; tr < toReverse.size() ; tr++){
			String toRev = toReverse.get(tr);
			int reveredNum = revered.size()-1-tr;
			String rev = revered.get(reveredNum);
			Assert.assertEquals(toRev, rev);
	
		}
	}
	
	@Test
	public void testReverseIntList() {
		List<Integer> toReverse = new ArrayList<Integer>();
		toReverse.add(9);
		toReverse.add(8);
		toReverse.add(4);
		toReverse.add(33);
		toReverse.add(111);
		toReverse.add(12);
		
		List<Integer> revered = ListHelper.ReverseList(toReverse);
		
		Assert.assertEquals(revered.size(), toReverse.size());
		
		for(int tr = 0 ; tr < toReverse.size() ; tr++){
			Integer toRev = toReverse.get(tr);
			int reveredNum = revered.size()-1-tr;
			Integer rev = revered.get(reveredNum);
			Assert.assertEquals(toRev, rev);
	
		}
	}

}
