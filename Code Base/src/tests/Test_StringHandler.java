package tests;

import org.junit.Assert;
import org.junit.Test;

import common.StringHandler;

public class Test_StringHandler {

	public final String target = "gonzo the great";
	@Test
	public void LeadSpaceRemover() {
		String underTest = "      Gonzo The Great";
		Assert.assertEquals(StringHandler.PrepareNameForCompare(underTest),target);
	}
	
	@Test
	public void TrailingSpaceRemover() {
		String underTest = "Gonzo The Great      ";
		Assert.assertEquals(StringHandler.PrepareNameForCompare(underTest),target);
	}

	@Test
	public void MultipleSpaceReplacer(){
		String underTest = "Gonzo    The       Great";
		Assert.assertEquals(StringHandler.PrepareNameForCompare(underTest),target);
	}
	
	@Test
	public void CaseRemover(){
		String underTest = "GoNzO ThE GrEaT";
		Assert.assertEquals(StringHandler.PrepareNameForCompare(underTest),target);
	}
	
}
