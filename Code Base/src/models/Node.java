package models;

import java.util.ArrayList;
import java.util.List;

public class Node implements iNode {
	
	private int Id;
	private String Name;
	
	private List<iNode> childNodes = new ArrayList<iNode>();
	private List<iNode> parentNodes = new ArrayList<iNode>();
	
	public Node(String _name, int _Id) {
		Name = _name;
		Id = _Id;
	}
	@Override
	public int getId() {
		return Id;
	}
	@Override
	public String getName() {
		return Name;
	}

	@Override
	public void addParentNode(iNode node) {
		if(node != null)
			parentNodes.add(node);
	}

	@Override
	public void addChildNode(iNode node) {
		if(node != null)
			childNodes.add(node);
	}
	
	@Override
	public List<iNode> getChildNodes() {
		return childNodes;
	}
	
	@Override
	public List<iNode> getParentNodes() {
		return parentNodes;
	}
	@Override
	public boolean hasSpecificChildNode(iNode node) {
		if(getChildNodes().contains(node))
			return true;
		return false;
	}
	@Override
	public boolean hasSpecificParentNode(iNode node) {
		if(getParentNodes().contains(node))
			return true;
		return false;
	}
	@Override
	public List<iNode> getAllAttachedNodes() {
		List<iNode> nodes = new ArrayList<iNode>();
		nodes.addAll(getChildNodes());
		nodes.addAll(getParentNodes());
		return nodes;
	}
	
	@Override
	public String toString(){
		return getName() + "(" + getId() + ")";
	}
	
}
