package models;

/**
 * @author Liam McCann
 * 
 *         Holds details of the source and destination of a relationship within
 *         the graph
 *
 */
public class Edge implements iEdge {

	private int Source;

	private int Destination;

	public Edge(int _source, int _destination) {
		Source = _source;
		Destination = _destination;
	}

	@Override
	public int getId() {
		return Source;
	}

	@Override
	public int getDestinationId() {
		return Destination;
	}
}
