package models;

public class Employee implements iEmployee{
	
	private String Name;
	private int Id;
	public Integer ManagerId;
	
	public Employee(String _name, int _Id, Integer _ManagerId) {
		Name = _name;
		Id = _Id;
		ManagerId = _ManagerId;
	}
	@Override
	public String getName() {
		return Name;
	}
	@Override
	public int getId() {
		return Id;
	}
	@Override
	public Integer getManagerId() {
		return ManagerId;
	}
}
