package models;

import java.util.List;

/**
 * @author njb11185
 *
 */
public interface iNode {
	public int getId();

	public String getName();

	/**
	 * Add's node to child node list
	 * 
	 * @param node
	 *            Node to add
	 */
	public void addChildNode(iNode node);

	/**
	 * Add's node to parent node list
	 * 
	 * @param node
	 *            Node to add
	 */
	public void addParentNode(iNode node);

	/**
	 * @return List of child nodes
	 */
	public List<iNode> getChildNodes();

	/**
	 * @return List of parent nodes
	 */
	public List<iNode> getParentNodes();

	/**
	 * @return List of parent and child nodes combined
	 */
	public List<iNode> getAllAttachedNodes();

	/**
	 * @param node
	 *            to find
	 * @return True if node is found within child list
	 */
	public boolean hasSpecificChildNode(iNode node);

	/**
	 * @param node
	 *            Node to find
	 * @return True if node is found within parent list
	 */
	public boolean hasSpecificParentNode(iNode node);
}
