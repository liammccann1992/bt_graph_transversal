package models;

/**
 * @author Liam McCann
 * 
 * Allows the system to get required information from employee class
 *
 */
public interface iEmployee{
	public String getName();
	public int getId();
	public Integer getManagerId();
}
