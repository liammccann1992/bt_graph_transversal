package models;

public interface iEdge {
	public int getId();
	public int getDestinationId();
}
