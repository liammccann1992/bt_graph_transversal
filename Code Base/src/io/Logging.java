package io;

public class Logging {
	private static boolean ShowLogs = false;
	
	public static void SetShowLogs(boolean set){
		ShowLogs = set;
	}
	
	public static void LogLn(String s){
		if(ShowLogs)
			System.out.println(s);
	}
	
	public static void Log(String s){
		if(ShowLogs)
			System.out.print(s);
	}
}
