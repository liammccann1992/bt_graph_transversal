package io;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * @author Liam McCann
 * 
 * Handles all communication with the file system
 *
 */
public class FileHandler {

	/**
	 * @param filename Path of file to  be opened
	 * @return The file that has been opened
	 * @throws NullPointerException When file path is null
	 * 
	 * Simple function to open a file
	 */
	public File openFile(String filename) throws NullPointerException {
		Logging.LogLn("Opening file: " + filename);
		return new File(filename);
	}

	/**
	 * @param filename Path of file to  be opened and parsed
	 * @return List<String> which are the parsed lines from the file
	 * @throws FileNotFoundException Thrown when file could not be found
	 * 
	 * Opens files and returns list of lines
	 */
	public List<String> ParseFile(String filename) throws FileNotFoundException {
		File toParse = openFile(filename);

		Scanner scanner = new Scanner(toParse);

		List<String> lines = new ArrayList<String>();
		Logging.LogLn("File opened parsing line by line");
		while (scanner.hasNextLine()) {
			String line = scanner.nextLine();
			Logging.LogLn("Line Read: " + line);
			lines.add(line);
		}
		//scanner.close();
		return lines;

		
	}
}
